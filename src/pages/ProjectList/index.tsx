import { useState, useEffect } from "react";
import Search from "./search";
import List from "./list";

import { cleanObject, useMounted, useDebounce } from "utils/index";
import { useHttp } from "utils/http";

export default function ProjectList() {
  const [param, setParam] = useState({
    name: "",
    personId: "",
  });
  const debouncedParam = useDebounce(param, 200);
  const [users, setUsers] = useState([]);
  const [list, setList] = useState([]);

  const client = useHttp();

  useEffect(() => {
    client("projects", { data: cleanObject(debouncedParam) }).then(setList);
  }, [debouncedParam]);

  useMounted(() => {
    client("users").then(setUsers);
  });

  return (
    <div>
      <Search users={users} param={param} setParam={setParam} />
      <List users={users} list={list} />
    </div>
  );
}
