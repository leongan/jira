import { User } from "types/user";

interface SearchProps {
  users: User[];
  param: {
    name: string;
    personId: string;
  };
  setParam: (param: SearchProps["param"]) => void;
}

export default function Search({ users, param, setParam }: SearchProps) {
  return (
    <form>
      <div>
        <input
          type="text"
          value={param.name}
          onChange={(e) =>
            setParam({
              ...param,
              name: e.target.value,
            })
          }
        />

        <select
          value={param.personId}
          onChange={(e) =>
            setParam({
              ...param,
              personId: e.target.value,
            })
          }
        >
          <option value={""}>负责人</option>
          {users.map((user) => {
            return (
              <option key={user.id} value={user.id}>
                {user.name}
              </option>
            );
          })}
        </select>
      </div>
    </form>
  );
}
