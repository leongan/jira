import { useEffect, useState } from "react";

/**
 * 判断值是否为 false，即排除把 0 当做 false 的情况
 * @param {*} value
 * @returns
 */
export const isFalsy = (value: unknown) => (value === 0 ? false : !value);

/**
 * 删除对象中值为 false 的 key
 * @param {*} object
 * @returns
 */
export const cleanObject = (object: object) => {
  // 这里展开 object 是做了一个拷贝，否则引用传递下会影响函数外的值
  const result = { ...object };
  Object.keys(result).forEach((key) => {
    // @ts-ignore
    if (isFalsy(result[key])) {
      // @ts-ignore
      delete result[key];
    }
  });
  return result;
};

/**
 * 组件挂载时执行逻辑
 * @param {*} callback
 */
export const useMounted = (callback: () => void) => {
  useEffect(() => {
    callback();
  }, []);
};

/**
 * 防止键盘输入时不断请求后端
 * @param {*} value
 * @param {*} delay
 * @returns
 */
export const useDebounce = <V>(value: V, delay?: number): V => {
  // 将传入的 value 转换为 state，因为 state 是响应式的，可以配合 effect 使用
  const [debouncedValue, setDebouncedValue] = useState(value);
  // 当 value 或者 delay 的值改变时触发
  useEffect(() => {
    // 设置一个定时器将 value 的值赋值给内部的 debouncedValue
    const timeout = setTimeout(() => setDebouncedValue(value), delay);
    // 当下次执行当前这个 effect 时（即 value 或 delay 变化时）调用这个函数进行清理工作
    return () => clearTimeout(timeout);
  }, [value, delay]);
  return debouncedValue;
};
