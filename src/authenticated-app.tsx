import React from "react";
import { useAuth } from "context/auth-context";
import ProjectList from "./pages/ProjectList";

/**
 * 用户登录后显示的组件
 */
export default function AuthenticatedApp() {
  const { logout } = useAuth();
  return (
    <div>
      <button onClick={logout}>退出登录</button>
      <p />
      <ProjectList />
    </div>
  );
}
