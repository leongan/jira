import React, { useState } from "react";
import Register from "./register";
import Login from "./login";

/**
 * 用户没登录之前显示的页面，使用 state 来控制显示登录还是注册组件
 */
export default function UnauthenticatedApp() {
  const [isRegister, setIsRegister] = useState(false);
  return (
    <div>
      {isRegister ? <Register /> : <Login />}
      <button onClick={() => setIsRegister(!isRegister)}>
        切换到 {isRegister ? "登录" : "注册"}
      </button>
    </div>
  );
}
