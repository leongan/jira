# 项目配置

## 指定资源引用的基础地址

```json
{
  "compilerOptions": {
    "baseUrl": "./src"
  }
}
```

## Prettier

1. 安装

   ```shell
   yarn add --dev --exact prettier
   ```

2. 创建配置文件 `.prettierrc.json`

   ```json
   {}
   ```

3. 创建过滤配置文件 `.prettierignore`

   ```txt
   build
   coverage
   ```

4. 安装 lint-staged 在 git 提交时自动格式化代码

   ```shell
   npx mrm lint-staged
   ```

5. 修改 `.husky/pre-commit` 文件

   ```txt
   #!/bin/sh
   . "$(dirname "$0")/_/husky.sh"

   # npx lint-staged # 去掉此行
   yarn lint-staged # 修改为 yarn，如果不用 yarn 可以不改

   ```

6. `package.json` 中配置 git 提交时要执行代码格式化的文件

   ```json
   {
     "lint-staged": {
       "*.{js,css,md,ts,tsx}": "prettier --write" # 增加 ts 和 tsx
     }
   }
   ```

7. 安装插件解决 Prettier 和 其他代码格式化插件的冲突

   ```shell
   yarn add eslint-config-prettier -D
   ```

8. 配置 ESlint 使用 Prettier 进行代码格式化

   ```json
   "eslintConfig": {
     "extends": [
       "react-app",
       "react-app/jest",
       "prettier" # 新增这行
     ]
   }
   ```

## 配置 commitlint 规范 git 提交注释

1. 安装 commitlint

   ```shell
   yarn add -D @commitlint/{config-conventional,cli}
   ```

2. 创建配置文件

   ```shell
   echo "module.exports = {extends: ['@commitlint/config-conventional']}" > commitlint.config.js
   npx husky add .husky/commit-msg "yarn commitlint --edit $1"
   ```

3. 规范说明

   - 提交信息必须以下列内容开头

     ```json
     [
       'build',
       'chore',
       'ci',
       'docs',
       'feat',
       'fix',
       'perf',
       'refactor',
       'revert',
       'style',
       'test'
     ];
     ```

   - 示例

     ```shell
     echo "foo: some message" # 错误
     echo "fix: some message" # 正确
     ```

# json-server 配置

1. 安装

   ```shell
   yarn add json-server -D
   ```

2. 在根目录创建目录 `__json_server_mock__`，并在目录内创建数据文件 `db.json`

   ```json
   {
     "users": []
   }
   ```

3. 在 package.json 中配置 json-server 启动脚本

   ```json
   "scripts": {
     "json-server": "json-server __json_server_mock__/db.json --watch"
   },
   ```

4. 启动 json-server 后根据控制台提示进行 Restful API 访问

# 用到的库

## qs

1. 作用

   对请求的 `querystring` 进行操作。[官方说明](https://www.npmjs.com/package/qs)

2. 安装

   ```shell
   yarn add qs
   ```

3. 安装 ts 类型声明

   ```shell
   yarn add @types/qs -D
   ```

4. 使用

   ```javascript
   import qs from "qs";
   qs.stringify(object); // 将对象转换为 querystring 的 key=value&key=value 形式
   ```
